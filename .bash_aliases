alias k='kubectl'
alias f='firefox'
alias h='helm'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

#alias indeeworkvpn='sudo openvpn --config ~/openvpn/client_sascharetter.ovpn'