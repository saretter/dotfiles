#! /bin/bash
{
  file=<backup-mount>/$(hostname)/${USER}
  if [ -d "${file}" ]; then
    date=$(date -d "today" +"%Y%m%d")
    restic --verbose -p ${HOME}/.resticcredentials -r <backup-mount>/$(hostname)/$(whoami) backup ${HOME}
    echo "[`date +%Y-%m-%d_%H-%M-%S`] Backup completed"
    restic --verbose -p ${HOME}/.resticcredentials -r <backup-mount>/$(hostname)/$(whoami) forget --keep-within-hourly 24h --keep-within-daily 7d --keep-within-weekly 1m --keep-within-monthly 1y --prune
    echo "[`date +%Y-%m-%d_%H-%M-%S`] Purging stale snapshots completed"
  else 
    echo "Warning repo ${file} not available / share mounted ?"
  fi
} >> ${HOME}/.backup.log 2>&1
