# My dotfiles

Based on documentation https://fwuensche.medium.com/how-to-manage-your-dotfiles-with-git-f7aeed8adf8b 

These dotfiles use Trueline to spice-up the terminal and install kubectl, helm, nextcloud-desktop.

Following steps are required on a new device.

## Install Nerdfont

Trueline.sh (https://github.com/petobens/trueline) is used to spice-up the promt-style. Therefore it is required to install a nerd-font (https://github.com/ryanoasis/nerd-fonts).

### Set font in Gnome Terminal profile
You need to choose the nerd-font in Gnome Terminal as `Custom Font` for the used profile (Edit > Preferences # Profile e.g. unnamed).

### Set font in VS Code 
Open User settings (JSON) using `strg + shift + P` and add e.g. `terminal.integrated.fontFamily: "UbuntuMono Nerd Font Mono"`

## Checkout and use dotfiles

```bash
git clone --bare https://codeberg.org/saretter/dotfiles.git $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles checkout
```
## Addons

The following addons might be used or not. They need to be explicitly executed.

### Initialize Backup 

This addon installs and configures components to backup the home-directory to a restic-reposity on a smb-share. 

Following steps are executed:

1. Create file containing smb-credentials in home-directory
2. Add line in fstab to mount smb-share
3. Mount smb-share
4. Create env-variable `BACKUP_MOUNT`
5. Create file containing restic-password in home-directory
6. Install and upgrade restic
7. Create restic-repository
8. Create cronjob