#!/bin/bash
#
# (c) Copyright 2022 Sascha Retter, All Rights Reserved

PARAMS=""
VERSION=0.1.1

# Determine information
this_path=$(readlink -f $0)
dir_name=`dirname ${this_path}`
myname=`basename ${this_path}`

function usage() {
   echo "
     usage: $myname [options]
     --------------------------------------------------------------------------------
     This script is used to configure backup of homedirectory on any linux-machine. 

     It requires a smb-share to host the restic-repository.
  
     This script generates:
       * ~/.backup.sh
       * ~/.smbcredentials
       * ~/.resticpassword
     and creates a new line in:
       * /etc/fstab
     --------------------------------------------------------------------------------
     
     Options:
     --help                        optional     Print this help message
     -l | --smb-location           mandatory    
     -u | --smb-username           mandatory    Username of the user having permission to the SMB share
     -p | --smb-password           mandatory    Password of the smb-user
     -m | --mountpoint             mandatory   
     -r | --restic-password        mandatory
   "
   exit 1
}

while (("$#" )); do
  case "$1" in
    --help)
      usage
      exit 0
      ;;
    -l | --smb-location)
      SMB_LOCATION=$2
      shift 2
      ;;
    -u | --smb-username)
      SMB_USERNAME=$2
      shift 2
      ;;
    -p | --smb-password)
      SMB_PASSWORD=$2
      shift 2
      ;; 
    -m | --mountpoint)
      BACKUP_MOUNT=$2
      shift 2
      ;; 
    -r | --restic-password)
      RESTIC_PASSWORD=$2
      shift 2
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Error: unsupported flag $1" >&2
      usage
      exit 1
      ;;
    *) # positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

# set positional arguments properly
eval set -- "$PARAMS"

# validate if mandatory params have been set
if [ -z ${SMB_LOCATION+x} ]; then usage; exit 0; fi;
if [ -z ${SMB_USERNAME+x} ]; then usage; exit 0; fi;
if [ -z ${SMB_PASSWORD+x} ]; then usage; exit 0; fi;
if [ -z ${BACKUP_MOUNT+x} ]; then usage; exit 0; fi;
if [ -z ${RESTIC_PASSWORD+x} ]; then usage; exit 0; fi;

HOME_DIR=$HOME
SMB_CREDENTIALS_FILE=.smbcredentials
RESTIC_CREDENTIALS_FILE=.resticcredentials
BACKUP_SCRIPT_FILE=.backup.sh

# create .smbcredentials in home-dir
if [ ! -f $HOME_DIR/$SMB_CREDENTIALS_FILE ] 
then
    touch ${HOME_DIR}/${SMB_CREDENTIALS_FILE}
    chown ${USER}:${USER $HOME_DIR}/${SMB_CREDENTIALS_FILE}
    chmod 600 ${HOME_DIR}/${SMB_CREDENTIALS_FILE}
    echo "user=${SMB_USERNAME}" >> ${HOME_DIR}/${SMB_CREDENTIALS_FILE}
    echo "user=${SMB_PASSWORD}" >> ${HOME_DIR}/${SMB_CREDENTIALS_FILE}
    echo "Created file ${HOME_DIR}/${SMB_CREDENTIALS_FILE}"
fi

# create mount-point in fstab if not exists and mount dir
if ! grep -q 'Backup smb-share' /etc/fstab ; then
    echo "Add entry for backup smb-share to /etc/fstab (requires root-permission)"
    sudo echo '# Backup smb-share' >> /etc/fstab
    sudo echo "${SMB_LOCATION} ${BACKUP_MOUNT} cifs _netdev,auto,credentials=${HOME_DIR}/${SMB_CREDENTIALS_FILE},uid=${USER},gid=${USER} 0 0" >> /etc/fstab
    echo "Added line to for ${SMB_LOCATION} to /etc/fstab"
    sudo mount ${SMB_LOCATION}
    echo "export BACKUP_MOUNT=${BACKUP_MOUNT}" >> .bash_exports
    echo "Mounted ${SMB_LOCATION} to ${BACKUP_MOUNT}"
fi

# create restic-credentials in home-directory
if [ ! -f ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE} ] 
then
    touch ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}
    chown ${USER}:${USER} ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}
    chmod 600 ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}
    echo "${RESTIC_PASSWORD}" >> ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}
    echo "Created file ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}"
fi

# install restic and upgrade
echo "Install / upgrade restic (requires root-permission)"
sudo apt install -y restic
sudo restic self-update
echo "Installed and updated restic"

# initialize restic-repo if not exists mount-point/<hostname>/<user>
if [ ! -d ${BACKUP_MOUNT}/$(hostname)/${USER} ]
then
    restic init -r ${BACKUP_MOUNT}/$(hostname)/${USER} -p ${HOME_DIR}/${RESTIC_CREDENTIALS_FILE}
    echo "Initialized restic repository"
fi

# create cronjob
crontab -u ${USER} -l > crontabtmp
if ! grep -q ".backup.sh" crontabtmp ; then
    sed -i "s#<backup-mount>#${BACKUP_MOUNT}#" .backup.sh
    echo "*/30 * * * * ${HOME_DIR}/.backup.sh >> ${HOME_DIR}/.backup.log 2>&1" >> crontabtmp
    crontab -u ${USER} ./crontabtmp
    echo "Created crontab for backup (*/30 * * * *)"
fi
rm crontabtmp